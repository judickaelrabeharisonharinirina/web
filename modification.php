<?php
$mysqli = new mysqli("localhost", "root", "", "eglise");

if ($mysqli->connect_errno) {
    //echo "Erreur de la connexion : " . $mysqli->connect_error;
} else {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];

        if (isset($_POST['date']) && isset($_POST["check"]) && isset($_POST['antony']) && isset($_POST['n_rosia']) && isset($_POST["vola"])) {
            $date = $_POST['date'];
            $check = $_POST['check'];
            $antony = $_POST['antony'];
            $n_rosia = $_POST['n_rosia'];
            $vola = $_POST['vola'];
            if ($check == "miditra") {
                $sql_update = "UPDATE compteeglise SET date = ?, recu_miditra = ?, recu_mivoaka = ' ' , antony = ? , argent_miditra = ? , argent_mivoaka = ' ' WHERE id = ?";
                $stmt = $mysqli->prepare($sql_update);
                $stmt->bind_param("ssssi", $date, $n_rosia, $antony, $vola, $id);
            } elseif ($check == "mivoaka") {
                $sql_update = "UPDATE compteeglise SET date = ?, recu_miditra = ' ' , recu_mivoaka = ? , antony = ? , argent_miditra = ' ' , argent_mivoaka = ? WHERE id = ?";
                $stmt = $mysqli->prepare($sql_update);
                $stmt->bind_param("ssssi", $date, $n_rosia, $antony, $vola, $id);
            }



            if ($stmt->execute()) {
                echo "Les données de l'eglise ont été mises à jour avec succès.";
            } else {
                echo "Erreur lors de la mise à jour des données du compte de l'eglise : " . $stmt->error;
            }

            $stmt->close();
        } else {
            echo "Données manquantes pour la mise à jour du compte de l'eglise.";
        }
    } else {
        echo "ID pour le donnée non spécifié.";
    }

    $mysqli->close();
}
?>
