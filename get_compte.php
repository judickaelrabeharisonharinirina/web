<?php
$mysqli = new mysqli("localhost", "root", "", "eglise");
if ($mysqli->connect_errno) {
    // Gestion de l'erreur de connexion
    echo "Erreur de la connexion : " . $mysqli->connect_error;
} else {
    // Connexion réussie

    // Vérifiez si la variable 'id' est définie
    if (isset($_POST['id'])) {
        $id = $_POST['id'];

        // Préparez la requête avec un paramètre lié
        $sql_select = "SELECT date, recu_miditra ,recu_mivoaka, antony, argent_miditra, argent_mivoaka FROM compteeglise WHERE id = ?";
        $stmt = $mysqli->prepare($sql_select);
        $stmt->bind_param("i", $id); // "i" pour un entier

        // Exécutez la requête
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $student = $result->fetch_assoc();

            if ($student) {
                echo json_encode($student);
            } else {
                echo "Données non trouvé.";
            }
        } else {
            echo "Erreur lors de l'exécution de la requête : " . $stmt->error;
        }

        $stmt->close();
    } else {
        echo "Date des données non spécifié.";
    }

    $mysqli->close();
}
?>