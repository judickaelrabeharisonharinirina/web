<?php
$mysqli = new mysqli("localhost", "root", "", "eglise");

if ($mysqli->connect_errno) {
    echo "connexion erreur";
} else {
    echo "connexion reussi";
}

$date = isset($_POST["date"]) ? $_POST["date"] : '';
$antony = isset($_POST["antony"]) ? $_POST["antony"] : '';
$n_rosia = isset($_POST["n_rosia"]) ? $_POST["n_rosia"] : '';
$vola = isset($_POST["vola"]) ? $_POST["vola"] : '';

if (isset($_POST["check"])) {
    $check = $_POST["check"];

    if (!empty($date) && !empty($antony) && !empty($n_rosia) && !empty($vola)) {
        // Vérifier si la valeur de n_rosia existe déjà
        $sql_check_existing = "SELECT COUNT(*) AS count FROM compteeglise WHERE recu_miditra = ? OR recu_mivoaka = ?";
        $stmt_check = $mysqli->prepare($sql_check_existing);
        $stmt_check->bind_param("ss", $n_rosia, $n_rosia);
        $stmt_check->execute();
        $result = $stmt_check->get_result();
        $count = $result->fetch_assoc()['count'];

        if ($count > 0) {
            echo "La valeur de la recu existe déjà. Insertion annulée.";
        } else {
            if ($check == "miditra") {
                // Case "Miditra" est cochée
                $sql_insert = "INSERT INTO compteeglise(date, recu_miditra, argent_miditra, antony) VALUES (?, ?, ?, ?)";
                $stmt = $mysqli->prepare($sql_insert);
                $stmt->bind_param("ssss", $date, $n_rosia, $vola, $antony);
            } elseif ($check == "mivoaka") {
                // Case "Mivoaka" est cochée
                $sql_insert = "INSERT INTO compteeglise(date, recu_mivoaka, argent_mivoaka, antony) VALUES (?, ?, ?, ?)";
                $stmt = $mysqli->prepare($sql_insert);
                $stmt->bind_param("ssss", $date, $n_rosia, $vola, $antony);
            }

            if ($stmt->execute()) {
                echo "insertion réussie";
            } else {
                echo "insertion échouée: " . $stmt->error;
            }

            $stmt->close();
        }
    } else {
        echo "Erreur : Les champs obligatoires ne sont pas remplis";
    }
} else {
    echo "Erreur : Aucune case n' a été cochée (Miditra ou Mivoaka)";
    exit();
}
?>